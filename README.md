# Comunicacion_SAP_Creator

Prueba de comunicación entre SAP S4/Hana y Zoho creator mediante SAP Gateway Odata

##POST

## Problemas con el TOKEN.

Se genera bien pero recivo el error:
**Error: 403 CSRF token validation failed**.

Leyendo de este tipo de token:

the "Synchronizer token" pattern

El patrón **"Cookie-to-header token"** es establecer una Cookie una vez por sesión y hacer que JavaScript lea esa cookie y establezca un encabezado HTTP personalizado (a menudo llamado X-CSRF-TOKEN o X-XSRF-TOKEN o sólo XSRF-TOKEN) con ese valor. 
Cualquier petición enviará tanto la cabecera (establecida por Javascript) como la cookie (establecida por el navegador como una cabecera HTTP estándar) y entonces el servidor puede comprobar que el valor de la cabecera **X-CSRF-TOKEN coincide con el valor de la cabecera de la cookie.** 

**x-csrf-token:**

Se añade al encabezamiento de la solicitud para las solicitudes de ajax.
Cuando se utiliza laravel como backend. laravel comprueba este encabezado automáticamente y lo compara con los csrf válidos de la base de datos.

**x-xsrf-token:**


Se añade al encabezamiento de la solicitud para las solicitudes de ajax.

Las bibliotecas populares como Angular y Axios, obtienen automáticamente el valor de este encabezado de la cookie de xsrf-token y lo envían con cada petición.

Debido a que es popular, laravel crea esta cookie en cada respuesta.

así que cuando usas por ejemplo axios y laravel no necesitas hacer nada. El usuario sólo necesita estar registrado y luego usar el middleware de "autenticación" hará el trabajo.
Es una cadena más grande comparada con x-csrf-token porque las cookies están encriptadas en laravel.


Solución para haceptara el Token, agregue la Cooke que entrega cuando solicita el token
~~~
{
	"cookie": "SAP_SESSIONID_J4D_100=PT1C7CA-BRw3D3uhchZlQ9_ze-dZrBHqlS9CAQqOAAM%3d; path=/",
	"x-csrf-token": "wD9nF6ybQXobVJqief_4LQ==",
	"Content-Type": "application/json",
	"Authorization": "Basic XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX=="
}

~~~

## Problemas con:
Error while parsing an XML stream: 'value expected at 'd=%7B%22MA''.

Parece no entender el formato del Body.
~~~
{
	"d": {
		"MATNR": 700003,
		"maktx": "Disco duro",
		"numero": 45,
		"texto": "FDSDFGBRT",
		"type": "Y_ODATA_MATERIALES_SRV.Materiales"
	}
}
~~~

Encontré este post es muy bueno, aunque no soluciono .
[JSON POST & PUT (single & batch) on NetWeaver Gateway](https://blogs.sap.com/2013/04/15/json-post-put-on-gateway/)


## DEbugueando en este metodo

### METHOD /iwcor/if_ds_proc_entity_set~create_entity.

### de la clase:
/IWFND/CL_SODATA_PROCESSOR====CP

Toma los datos leidos en binario en:

~~~
class /IWCOR/CL_DS_EP_FACADE
method GET_SXML_READER.

~~~

#### String de entrada desde SAP
~~~
:::HEX
7B2264223A7B224D41544E52223A202237303030303037222C226D616B7478223A20224275727269746F20536162616E65726F222C226E756D65726F223A382C22746578746F223A20226163616C6F7261646F227D7D
~~~

~~~
:::ASCII
{"d":{"MATNR": "7000007","maktx": "Burrito Sabanero","numero":8,"texto": "acalorado"}}
~~~

#### String de entrada desde Creator

Datos enviados:
~~~
{"d":{"MATNR": "7000007","maktx": "Burrito Sabanero","numero":8,"texto": "acalorado"}}
~~~

~~~
:::HEX
643D253742253232746578746F2532322533412532326163616C6F7261646F2532322532432532326E756D65726F253232253341382532432532326D616B74782532322533412532324275727269746F2B536162616E65726F2532322532432532324D41544E5225323225334125323237303030303037253232253744
~~~

~~~
:::ASCII
d=%7B%22texto%22%3A%22acalorado%22%2C%22numero%22%3A8%2C%22maktx%22%3A%22Burrito+Sabanero%22%2C%22MATNR%22%3A%227000007%22%7D
~~~

#### String de entrada desde Posman

~~~
:::HEX
7B0D0A2020226422203A207B0D0A20202020224D41544E5222203A202237303030303039222C0D0A20202020226D616B747822203A20224275727269746F20536162616E65726F222C0D0A20202020226E756D65726F22203A20382C0D0A2020202022746578746F22203A2022506F73746D616E220D0A20207D0D0A7D

~~~

~~~
:::ASCII
{
  "d" : {
    "MATNR" : "7000009",
    "maktx" : "Burrito Sabanero",
    "numero" : 8,
    "texto" : "Postman"
  }
}
~~~
## Se soluciona con el Soporte
el Body deberia ser un String, para que no lo transforme

Ya funciona super bien
